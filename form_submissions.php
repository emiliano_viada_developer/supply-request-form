<?php

require_once "vendor/autoload.php";

class FormSubmission
{
	const FORM_ENABLED = 'ENABLED';
	const SUBMISSION_ACTIVE = 'ACTIVE';

	protected $jotformAPI;

	protected $config;

	protected $emailsTo;

	protected $mailer;

	protected $fieldsMapping = array('store' => 3, 'manager' => 4, 'requestedBy' => 1, 'medicalApp' => 7,
		'medicalAppQty' => 16, 'dentalApp' => 17, 'dentalAppQty' => 18, 'workersPacket' => 19, 'workersPacketQty' => 20,
		'policies' => 21, 'policiesQty' => 22, 'tick' => 23, 'tickQty' => 24, 'rhOther' => 27, 'rhComments' => 28,
		'businessCards' => 30, 'businessCardsQty' => 31, 'ccImprint' => 32, 'ccImprintQty' => 33, 'ccTerminal' => 34,
		'ccTerminalQty' => 35, 'register' => 36, 'registerQty' => 37, 'depositBags' => 38, 'depositBagsQty' => 39,
		'depositSlips' => 40, 'depositSlipsQty' => 41, 'manual' => 46, 'manualQty' => 47, 'stickers' => 48, 'stickersQty' => 49,
		'ofComments' => 42, 'salesFloor' => 50, 'lbModel' => 52, 'salesFloorQty' => 51, 'stockroom' => 54, 'slbModel' => 55,
		'stockroomQty' => 56, 'lbOther' => 43, 'lbComments' => 53, 'boxCutterBlades' => 102, 'boxCutterBladesQty' => 101,
		'boxCutterHandles' => 104, 'boxCutterHandlesQty' => 125, 'calculator' => 124, 'calculatorQty' => 126, 'envelopes' => 123,
		'envelopesQty' => 127, 'envelopes912' => 121, 'envelopes912Qty' => 137, 'greyBags' => 122, 'greyBagsQty' => 136,
		'highliter' => 120, 'highliterQty' => 135, 'markersJumbo' => 119,  'markersJumboQty' => 134, 'markersSharpie' => 118,
		'markersSharpieQty' => 133, 'notebooks' => 116, 'notebooksQty' => 132, 'packingEnvelopes' => 115, 'packingEnvelopesQty' => 131,
		'packingTape' => 114, 'packingTapeQty' => 128, 'paperPrinter' => 113, 'paperPrinterQty' => 129, 'paperClips' => 117,
		'paperClipsQty' => 138, 'pens' => 112, 'pensQty' => 139, 'rubberBands' => 111, 'rubberBandsQty' => 142, 'scissors' => 110,
		'scissorsQty' => 143, 'stapler' => 109, 'staplerQty' => 141, 'staples' => 108, 'staplesQty' => 144, 'storeBagsBig' => 107,
		'storeBagsBigQty' => 145, 'storeBagsSmall' => 106, 'storeBagsSmallQty' => 146, 'policySign' => 100, 'policySignQty' => 189,
		'scotchTape' => 105, 'scotchTapeQty' => 130, 'toner' => 147, 'tonerPrinterModel' => 154, 'tonerQty' => 152,
		'whiteOut' => 149, 'whiteOutQty' => 153, 'trySocks' => 148, 'trySocksQty' => 151, 'sOther' => 99, 'sComments' => 103,
		'airFreshener' => 150, 'airFreshenerQty' => 140, 'bathroomCleaner' => 156, 'bathroomCleanerQty' => 157, 'carpetCleaner' => 164,
		'carpetCleanerQty' => 180, 'handSoap' => 163, 'handSoapQty' => 179, 'handSanitizer' => 168, 'handSanitizerQty' => 177,
		'mopHead' => 167, 'mopHeadQty' => 176, 'mopHandle' => 166, 'mopHandleQty' => 174, 'paperTowels' => 165, 'paperTowelsQty' => 172,
		'pineSol' => 162, 'pineSolQty' => 171, 'toiletSeat' => 161, 'toiletSeatQty' => 170, 'toiletTissue' => 160, 'toiletTissueQty' => 178,
		'trashBags33' => 159, 'trashBags33Qty' => 175, 'trashBags13' => 158, 'trashBags13Qty' => 173, 'windex' => 181, 'windexQty' => 169,
		'jOther' => 45, 'jComments' => 182, 'bpt999' => 77, 'bpt999Qty' => 78, 'bpt2999' => 79, 'bpt2999Qty' => 80, 'bptComments' => 187,
		'st499' => 57, 'st499Qty' => 58, 'st5499' => 64, 'st5499Qty' => 63, 'blank' => 65, 'blankQty' => 62, 'outletValue' => 66,
		'outletValueQty' => 61, 'plasticTag' => 67, 'plasticTagQty' => 60, 'taggingGun' => 68, 'taggingGunQty' => 59,
		'gunNeedles' => 69, 'gunNeedlesQty' => 70, 'priceGun' => 73, 'priceGunQty' => 74, 'stOther' => 43, 'stComments' => 72,
		'blankPrice' => 84, 'blankPriceQty' => 85, 'stcOther' => 82, 'stcComments' => 83, 'sho999' => 87, 'sho999Qty' => 88,
		'sho1999' => 89, 'sho1999Qty' => 90, 'sho2999' => 91, 'sho2999Qty' => 96, 'sho3999' => 92, 'sho3999Qty' => 95, 'sho4999' => 93,
		'sho4999Qty' => 94, 'shoBlank' => 97, 'shoBlankQty' => 98, 'shoComments' => 71, 'specialComment' => 29, 'reference' => 192,
		'percOffUgg' => 195, 'percOffUggQty' => 196
	);

	protected $rowColors = array(false => 'white', true => '#e3e3e3');

	/**
	 * Constructor
	 */
	public function __construct()
	{
		date_default_timezone_set('America/New_York');
		$this->jotformAPI = new JotForm("f792e5f17cbd2648a321b511ae4a385b");
		$this->config = json_decode(file_get_contents(dirname(__FILE__) . '/config.json'), 1);
		$this->configureMailer();
	}

	/**
	 * configureMailer() function
	 */
	protected function configureMailer()
	{
		// Create the Transport
		$transport = Swift_SmtpTransport::newInstance('mail.robertwayne.com', 587)
		  	->setUsername('testrequest@robertwayne.com')
		  	->setPassword('Testrequest$')
	  	;
		// For Development
		/*$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
		  	->setUsername('dev.excedesoft@gmail.com')
		  	->setPassword('footable')
	  	;*/
		/*
		You could alternatively use a different transport such as Sendmail or Mail:
		// Sendmail
		$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
		// Mail
		$transport = Swift_MailTransport::newInstance();
		*/

		// Create the Mailer using your created Transport
		$this->mailer = Swift_Mailer::newInstance($transport);

		return;
	}

	/**
	 * run() function
	 */
	public function run()
	{
		$sends = 0;
		// Supply Requisition - Drop down - Final: http://form.jotformpro.com/form/51056745346962
		$formId = '51056745346962';
		$form = $this->jotformAPI->getForm($formId);

		if ($form['status'] === self::FORM_ENABLED) {
			// Get Form properties
			$properties = $this->jotformAPI->getFormProperties($formId);
			$this->emailsTo = $this->getEmailsToFromConditions($properties['conditions']);

			// Get submissions
			$filter = array(
		        "created_at:gt" => $this->config['last_execution'],
		    );
			$submissions = $this->jotformAPI->getFormSubmissions($formId, 0, 0, $filter);

		    if (count($submissions)) {
		    	foreach ($submissions as $k => $submission) {
		    		if ($submission['id'] != "#SampleSubmissionID") {
		    			// Process the submission
		    			$sent = $this->processSubmission($submission);
		    			if ($sent) {
		    				$sends++;
		    			}
		    		}
		    	}
		    }

			// Update last execution
		    $this->config['last_execution'] = date('Y-m-d H:i:s');
		    file_put_contents(dirname(__FILE__) . '/config.json', json_encode($this->config));

			echo($sends . " submissions were sent.\n");
		} else {
			echo("The '" . $form['title'] . "' form is not enabled.\n");
		}
	}

	/**
	 * getEmailsToFromConditions() function
	 * @param array $conditions
	 */
	protected function getEmailsToFromConditions($conditions)
	{
		$emails = array();
		if (count($conditions)) {
			foreach ($conditions as $k => $condition) {
				// Only consider 'email' conditions
				if ($condition['type'] == 'email') {
					$terms = json_decode($condition['terms'], 1);
					$action = json_decode($condition['action'], 1);
					$emails[$terms[0]['value']] = $action[0]['to'];
				}
			}
		}

		return $emails;
	}

	/**
	 * processSubmission() function
	 * @param array $submission
	 */
	protected function processSubmission($submission)
	{
		$status = $submission['status'];
		$answers = $submission['answers'];
		if ($status == self::SUBMISSION_ACTIVE) {
			// Get Email TO
			$emailTo = 'supplies@robertwayne.com';
//Hardcoded to send to development
//$emailTo = 'emiliano.viada@excedesoft.com';
			$manager = $answers[4]['answer'];
			$emailToCC = (isset($this->emailsTo[$manager]))? $this->emailsTo[$manager] : null;
//Hardcoded to send to development
//$emailToCC = 'emjovi@gmail.com';
			if (is_null($emailTo)) {
				echo("The email address for '" . $manager . "' is not specified.\n");

			} else {
				list($bodyHtml, $bodyText) = $this->getBody($answers);
				$subject = $answers[$this->fieldsMapping['store']]['answer'];
				$message = Swift_Message::newInstance()
					->setSubject($subject)
					->setFrom(array('noreply@jotform.com' => 'noreply@jotform.com'))
					->setTo(array($emailTo => $emailTo));

				if (!is_null($emailToCC)) {
					$message->setCc(array($emailToCC => $manager));
				}

				$message->setBody($bodyHtml, 'text/html')
					->addPart($bodyText, 'text/plain')
				;

				$result = $this->mailer->send($message);

				if ($result) {
					echo("Email sent to " . $emailTo . " (CC: ". $emailToCC .").\n");
				}
			}
		}

		return $result;
	}

	/**
	 * getBody() function
	 * @param array $answers
	 */
	protected function getBody($answers)
	{
		$bodies = array();
		$flag = false;

		// Get answers variables
		$store = $answers[$this->fieldsMapping['store']]['answer'];
		$manager = $answers[$this->fieldsMapping['manager']]['answer'];
		$requestedBy = $answers[$this->fieldsMapping['requestedBy']]['prettyFormat'];
		// Human Resources
		$medicalApp = (isset($answers[$this->fieldsMapping['medicalApp']]['answer']))? $answers[$this->fieldsMapping['medicalApp']]['answer'] : null;
		$medicalAppQty = (isset($answers[$this->fieldsMapping['medicalAppQty']]['answer']))? $answers[$this->fieldsMapping['medicalAppQty']]['answer'] : null;
		$dentalApp = (isset($answers[$this->fieldsMapping['dentalApp']]['answer']))? $answers[$this->fieldsMapping['dentalApp']]['answer'] : null;
		$dentalAppQty = (isset($answers[$this->fieldsMapping['dentalAppQty']]['answer']))? $answers[$this->fieldsMapping['dentalAppQty']]['answer'] : null;
		$workersPacket = (isset($answers[$this->fieldsMapping['workersPacket']]['answer']))? $answers[$this->fieldsMapping['workersPacket']]['answer'] : null;
		$workersPacketQty = (isset($answers[$this->fieldsMapping['workersPacketQty']]['answer']))? $answers[$this->fieldsMapping['workersPacketQty']]['answer'] : null;
		$policies = (isset($answers[$this->fieldsMapping['policies']]['answer']))? $answers[$this->fieldsMapping['policies']]['answer'] : null;
		$policiesQty = (isset($answers[$this->fieldsMapping['policiesQty']]['answer']))? $answers[$this->fieldsMapping['policiesQty']]['answer'] : null;
		$tick = (isset($answers[$this->fieldsMapping['tick']]['answer']))? $answers[$this->fieldsMapping['tick']]['answer'] : null;
		$tickQty = (isset($answers[$this->fieldsMapping['tickQty']]['answer']))? $answers[$this->fieldsMapping['tickQty']]['answer'] : null;
		$rhOther = (isset($answers[$this->fieldsMapping['rhOther']]['answer']))? $answers[$this->fieldsMapping['rhOther']]['answer'] : null;
		$rhComments = (isset($answers[$this->fieldsMapping['rhComments']]['answer']))? $answers[$this->fieldsMapping['rhComments']]['answer'] : null;
		$showHumanResources = ($medicalApp || $dentalApp || $workersPacket || $policies || $tick || $rhOther || $rhComments);
		// Operation Forms
		$businessCards = (isset($answers[$this->fieldsMapping['businessCards']]['answer']))? $answers[$this->fieldsMapping['businessCards']]['answer'] : null;
		$businessCardsQty = (isset($answers[$this->fieldsMapping['businessCardsQty']]['answer']))? $answers[$this->fieldsMapping['businessCardsQty']]['answer'] : null;
		$ccImprint = (isset($answers[$this->fieldsMapping['ccImprint']]['answer']))? $answers[$this->fieldsMapping['ccImprint']]['answer'] : null;
		$ccImprintQty = (isset($answers[$this->fieldsMapping['ccImprintQty']]['answer']))? $answers[$this->fieldsMapping['ccImprintQty']]['answer'] : null;
		$ccTerminal = (isset($answers[$this->fieldsMapping['ccTerminal']]['answer']))? $answers[$this->fieldsMapping['ccTerminal']]['answer'] : null;
		$ccTerminalQty = (isset($answers[$this->fieldsMapping['ccTerminalQty']]['answer']))? $answers[$this->fieldsMapping['ccTerminalQty']]['answer'] : null;
		$register = (isset($answers[$this->fieldsMapping['register']]['answer']))? $answers[$this->fieldsMapping['register']]['answer'] : null;
		$registerQty = (isset($answers[$this->fieldsMapping['registerQty']]['answer']))? $answers[$this->fieldsMapping['registerQty']]['answer'] : null;
		$depositBags = (isset($answers[$this->fieldsMapping['depositBags']]['answer']))? $answers[$this->fieldsMapping['depositBags']]['answer'] : null;
		$depositBagsQty = (isset($answers[$this->fieldsMapping['depositBagsQty']]['answer']))? $answers[$this->fieldsMapping['depositBagsQty']]['answer'] : null;
		$depositSlips = (isset($answers[$this->fieldsMapping['depositSlips']]['answer']))? $answers[$this->fieldsMapping['depositSlips']]['answer'] : null;
		$depositSlipsQty = (isset($answers[$this->fieldsMapping['depositSlipsQty']]['answer']))? $answers[$this->fieldsMapping['depositSlipsQty']]['answer'] : null;
		$manual = (isset($answers[$this->fieldsMapping['manual']]['answer']))? $answers[$this->fieldsMapping['manual']]['answer'] : null;
		$manualQty = (isset($answers[$this->fieldsMapping['manualQty']]['answer']))? $answers[$this->fieldsMapping['manualQty']]['answer'] : null;
		$stickers = (isset($answers[$this->fieldsMapping['stickers']]['answer']))? $answers[$this->fieldsMapping['stickers']]['answer'] : null;
		$stickersQty = (isset($answers[$this->fieldsMapping['stickersQty']]['answer']))? $answers[$this->fieldsMapping['stickersQty']]['answer'] : null;
		$ofComments = (isset($answers[$this->fieldsMapping['ofComments']]['answer']))? $answers[$this->fieldsMapping['ofComments']]['answer'] : null;
		$showOperationForms = ($businessCards || $ccImprint || $ccTerminal || $register || $depositBags || $depositSlips || $manual || $stickers || $ofComments);
		// Light Bulbs
		$salesFloor = (isset($answers[$this->fieldsMapping['salesFloor']]['answer']))? $answers[$this->fieldsMapping['salesFloor']]['answer'] : null;
		$lbModel = (isset($answers[$this->fieldsMapping['lbModel']]['answer']))? $answers[$this->fieldsMapping['lbModel']]['answer'] : null;
		$salesFloorQty = (isset($answers[$this->fieldsMapping['salesFloorQty']]['answer']))? $answers[$this->fieldsMapping['salesFloorQty']]['answer'] : null;
		$stockroom = (isset($answers[$this->fieldsMapping['stockroom']]['answer']))? $answers[$this->fieldsMapping['stockroom']]['answer'] : null;
		$slbModel = (isset($answers[$this->fieldsMapping['slbModel']]['answer']))? $answers[$this->fieldsMapping['slbModel']]['answer'] : null;
		$stockroomQty = (isset($answers[$this->fieldsMapping['stockroomQty']]['answer']))? $answers[$this->fieldsMapping['stockroomQty']]['answer'] : null;
		$lbOther = (isset($answers[$this->fieldsMapping['lbOther']]['answer']))? $answers[$this->fieldsMapping['lbOther']]['answer'] : null;
		$lbComments = (isset($answers[$this->fieldsMapping['lbComments']]['answer']))? $answers[$this->fieldsMapping['lbComments']]['answer'] : null;
		$showLightBulbs = ($salesFloor || $stockroom || $lbOther || $lbComments);
		// Supplies
		$boxCutterBlades = (isset($answers[$this->fieldsMapping['boxCutterBlades']]['answer']))? $answers[$this->fieldsMapping['boxCutterBlades']]['answer'] : null;
		$boxCutterBladesQty = (isset($answers[$this->fieldsMapping['boxCutterBladesQty']]['answer']))? $answers[$this->fieldsMapping['boxCutterBladesQty']]['answer'] : null;
		$boxCutterHandles = (isset($answers[$this->fieldsMapping['boxCutterHandles']]['answer']))? $answers[$this->fieldsMapping['boxCutterHandles']]['answer'] : null;
		$boxCutterHandlesQty = (isset($answers[$this->fieldsMapping['boxCutterHandlesQty']]['answer']))? $answers[$this->fieldsMapping['boxCutterHandlesQty']]['answer'] : null;
		$calculator = (isset($answers[$this->fieldsMapping['calculator']]['answer']))? $answers[$this->fieldsMapping['calculator']]['answer'] : null;
		$calculatorQty = (isset($answers[$this->fieldsMapping['calculatorQty']]['answer']))? $answers[$this->fieldsMapping['calculatorQty']]['answer'] : null;
		$envelopes = (isset($answers[$this->fieldsMapping['envelopes']]['answer']))? $answers[$this->fieldsMapping['envelopes']]['answer'] : null;
		$envelopesQty = (isset($answers[$this->fieldsMapping['envelopesQty']]['answer']))? $answers[$this->fieldsMapping['envelopesQty']]['answer'] : null;
		$envelopes912 = (isset($answers[$this->fieldsMapping['envelopes912']]['answer']))? $answers[$this->fieldsMapping['envelopes912']]['answer'] : null;
		$envelopes912Qty = (isset($answers[$this->fieldsMapping['envelopes912Qty']]['answer']))? $answers[$this->fieldsMapping['envelopes912Qty']]['answer'] : null;
		$greyBags = (isset($answers[$this->fieldsMapping['greyBags']]['answer']))? $answers[$this->fieldsMapping['greyBags']]['answer'] : null;
		$greyBagsQty = (isset($answers[$this->fieldsMapping['greyBagsQty']]['answer']))? $answers[$this->fieldsMapping['greyBagsQty']]['answer'] : null;
		$highliter = (isset($answers[$this->fieldsMapping['highliter']]['answer']))? $answers[$this->fieldsMapping['highliter']]['answer'] : null;
		$highliterQty = (isset($answers[$this->fieldsMapping['highliterQty']]['answer']))? $answers[$this->fieldsMapping['highliterQty']]['answer'] : null;
		$markersJumbo = (isset($answers[$this->fieldsMapping['markersJumbo']]['answer']))? $answers[$this->fieldsMapping['markersJumbo']]['answer'] : null;
		$markersJumboQty = (isset($answers[$this->fieldsMapping['markersJumboQty']]['answer']))? $answers[$this->fieldsMapping['markersJumboQty']]['answer'] : null;
		$markersSharpie = (isset($answers[$this->fieldsMapping['markersSharpie']]['answer']))? $answers[$this->fieldsMapping['markersSharpie']]['answer'] : null;
		$markersSharpieQty = (isset($answers[$this->fieldsMapping['markersSharpieQty']]['answer']))? $answers[$this->fieldsMapping['markersSharpieQty']]['answer'] : null;
		$notebooks = (isset($answers[$this->fieldsMapping['notebooks']]['answer']))? $answers[$this->fieldsMapping['notebooks']]['answer'] : null;
		$notebooksQty = (isset($answers[$this->fieldsMapping['notebooksQty']]['answer']))? $answers[$this->fieldsMapping['notebooksQty']]['answer'] : null;
		$packingEnvelopes = (isset($answers[$this->fieldsMapping['packingEnvelopes']]['answer']))? $answers[$this->fieldsMapping['packingEnvelopes']]['answer'] : null;
		$packingEnvelopesQty = (isset($answers[$this->fieldsMapping['packingEnvelopesQty']]['answer']))? $answers[$this->fieldsMapping['packingEnvelopesQty']]['answer'] : null;
		$packingTape = (isset($answers[$this->fieldsMapping['packingTape']]['answer']))? $answers[$this->fieldsMapping['packingTape']]['answer'] : null;
		$packingTapeQty = (isset($answers[$this->fieldsMapping['packingTapeQty']]['answer']))? $answers[$this->fieldsMapping['packingTapeQty']]['answer'] : null;
		$paperPrinter = (isset($answers[$this->fieldsMapping['paperPrinter']]['answer']))? $answers[$this->fieldsMapping['paperPrinter']]['answer'] : null;
		$paperPrinterQty = (isset($answers[$this->fieldsMapping['paperPrinterQty']]['answer']))? $answers[$this->fieldsMapping['paperPrinterQty']]['answer'] : null;
		$paperClips = (isset($answers[$this->fieldsMapping['paperClips']]['answer']))? $answers[$this->fieldsMapping['paperClips']]['answer'] : null;
		$paperClipsQty = (isset($answers[$this->fieldsMapping['paperClipsQty']]['answer']))? $answers[$this->fieldsMapping['paperClipsQty']]['answer'] : null;
		$pens = (isset($answers[$this->fieldsMapping['pens']]['answer']))? $answers[$this->fieldsMapping['pens']]['answer'] : null;
		$pensQty = (isset($answers[$this->fieldsMapping['pensQty']]['answer']))? $answers[$this->fieldsMapping['pensQty']]['answer'] : null;
		$rubberBands = (isset($answers[$this->fieldsMapping['rubberBands']]['answer']))? $answers[$this->fieldsMapping['rubberBands']]['answer'] : null;
		$rubberBandsQty = (isset($answers[$this->fieldsMapping['rubberBandsQty']]['answer']))? $answers[$this->fieldsMapping['rubberBandsQty']]['answer'] : null;
		$scissors = (isset($answers[$this->fieldsMapping['scissors']]['answer']))? $answers[$this->fieldsMapping['scissors']]['answer'] : null;
		$scissorsQty = (isset($answers[$this->fieldsMapping['scissorsQty']]['answer']))? $answers[$this->fieldsMapping['scissorsQty']]['answer'] : null;
		$stapler = (isset($answers[$this->fieldsMapping['stapler']]['answer']))? $answers[$this->fieldsMapping['stapler']]['answer'] : null;
		$staplerQty = (isset($answers[$this->fieldsMapping['staplerQty']]['answer']))? $answers[$this->fieldsMapping['staplerQty']]['answer'] : null;
		$staples = (isset($answers[$this->fieldsMapping['staples']]['answer']))? $answers[$this->fieldsMapping['staples']]['answer'] : null;
		$staplesQty = (isset($answers[$this->fieldsMapping['staplesQty']]['answer']))? $answers[$this->fieldsMapping['staplesQty']]['answer'] : null;
		$storeBagsBig = (isset($answers[$this->fieldsMapping['storeBagsBig']]['answer']))? $answers[$this->fieldsMapping['storeBagsBig']]['answer'] : null;
		$storeBagsBigQty = (isset($answers[$this->fieldsMapping['storeBagsBigQty']]['answer']))? $answers[$this->fieldsMapping['storeBagsBigQty']]['answer'] : null;
		$storeBagsSmall = (isset($answers[$this->fieldsMapping['storeBagsSmall']]['answer']))? $answers[$this->fieldsMapping['storeBagsSmall']]['answer'] : null;
		$storeBagsSmallQty = (isset($answers[$this->fieldsMapping['storeBagsSmallQty']]['answer']))? $answers[$this->fieldsMapping['storeBagsSmallQty']]['answer'] : null;
		$policySign = (isset($answers[$this->fieldsMapping['policySign']]['answer']))? $answers[$this->fieldsMapping['policySign']]['answer'] : null;
		$policySignQty = (isset($answers[$this->fieldsMapping['policySignQty']]['answer']))? $answers[$this->fieldsMapping['policySignQty']]['answer'] : null;
		$scotchTape = (isset($answers[$this->fieldsMapping['scotchTape']]['answer']))? $answers[$this->fieldsMapping['scotchTape']]['answer'] : null;
		$scotchTapeQty = (isset($answers[$this->fieldsMapping['scotchTapeQty']]['answer']))? $answers[$this->fieldsMapping['scotchTapeQty']]['answer'] : null;
		$toner = (isset($answers[$this->fieldsMapping['toner']]['answer']))? $answers[$this->fieldsMapping['toner']]['answer'] : null;
		$tonerPrinterModel = (isset($answers[$this->fieldsMapping['tonerPrinterModel']]['answer']))? $answers[$this->fieldsMapping['tonerPrinterModel']]['answer'] : null;
		$tonerQty = (isset($answers[$this->fieldsMapping['tonerQty']]['answer']))? $answers[$this->fieldsMapping['tonerQty']]['answer'] : null;
		$whiteOut = (isset($answers[$this->fieldsMapping['whiteOut']]['answer']))? $answers[$this->fieldsMapping['whiteOut']]['answer'] : null;
		$whiteOutQty = (isset($answers[$this->fieldsMapping['whiteOutQty']]['answer']))? $answers[$this->fieldsMapping['whiteOutQty']]['answer'] : null;
		$trySocks = (isset($answers[$this->fieldsMapping['trySocks']]['answer']))? $answers[$this->fieldsMapping['trySocks']]['answer'] : null;
		$trySocksQty = (isset($answers[$this->fieldsMapping['trySocksQty']]['answer']))? $answers[$this->fieldsMapping['trySocksQty']]['answer'] : null;
		$sOther = (isset($answers[$this->fieldsMapping['sOther']]['answer']))? $answers[$this->fieldsMapping['sOther']]['answer'] : null;
		$sComments = (isset($answers[$this->fieldsMapping['sComments']]['answer']))? $answers[$this->fieldsMapping['sComments']]['answer'] : null;
		$showSupplies = ($boxCutterBlades || $boxCutterHandles || $calculator || $envelopes || $envelopes912 || $greyBags || $highliter ||
			$markersJumbo || $markersSharpie || $notebooks || $packingEnvelopes || $packingTape || $paperPrinter || $paperClips || $pens ||
			$rubberBands || $scissors || $stapler || $staples || $storeBagsBig || $storeBagsSmall || $policySign || $scotchTape || $toner ||
			$whiteOut || $trySocks || $sOther || $sComments);
		// Janitorial
		$airFreshener = (isset($answers[$this->fieldsMapping['airFreshener']]['answer']))? $answers[$this->fieldsMapping['airFreshener']]['answer'] : null;
		$airFreshenerQty = (isset($answers[$this->fieldsMapping['airFreshenerQty']]['answer']))? $answers[$this->fieldsMapping['airFreshenerQty']]['answer'] : null;
		$bathroomCleaner = (isset($answers[$this->fieldsMapping['bathroomCleaner']]['answer']))? $answers[$this->fieldsMapping['bathroomCleaner']]['answer'] : null;
		$bathroomCleanerQty = (isset($answers[$this->fieldsMapping['bathroomCleanerQty']]['answer']))? $answers[$this->fieldsMapping['bathroomCleanerQty']]['answer'] : null;
		$carpetCleaner = (isset($answers[$this->fieldsMapping['carpetCleaner']]['answer']))? $answers[$this->fieldsMapping['carpetCleaner']]['answer'] : null;
		$carpetCleanerQty = (isset($answers[$this->fieldsMapping['carpetCleanerQty']]['answer']))? $answers[$this->fieldsMapping['carpetCleanerQty']]['answer'] : null;
		$handSoap = (isset($answers[$this->fieldsMapping['handSoap']]['answer']))? $answers[$this->fieldsMapping['handSoap']]['answer'] : null;
		$handSoapQty = (isset($answers[$this->fieldsMapping['handSoapQty']]['answer']))? $answers[$this->fieldsMapping['handSoapQty']]['answer'] : null;
		$handSanitizer = (isset($answers[$this->fieldsMapping['handSanitizer']]['answer']))? $answers[$this->fieldsMapping['handSanitizer']]['answer'] : null;
		$handSanitizerQty = (isset($answers[$this->fieldsMapping['handSanitizerQty']]['answer']))? $answers[$this->fieldsMapping['handSanitizerQty']]['answer'] : null;
		$mopHead = (isset($answers[$this->fieldsMapping['mopHead']]['answer']))? $answers[$this->fieldsMapping['mopHead']]['answer'] : null;
		$mopHeadQty = (isset($answers[$this->fieldsMapping['mopHeadQty']]['answer']))? $answers[$this->fieldsMapping['mopHeadQty']]['answer'] : null;
		$mopHandle = (isset($answers[$this->fieldsMapping['mopHandle']]['answer']))? $answers[$this->fieldsMapping['mopHandle']]['answer'] : null;
		$mopHandleQty = (isset($answers[$this->fieldsMapping['mopHandleQty']]['answer']))? $answers[$this->fieldsMapping['mopHandleQty']]['answer'] : null;
		$paperTowels = (isset($answers[$this->fieldsMapping['paperTowels']]['answer']))? $answers[$this->fieldsMapping['paperTowels']]['answer'] : null;
		$paperTowelsQty = (isset($answers[$this->fieldsMapping['paperTowelsQty']]['answer']))? $answers[$this->fieldsMapping['paperTowelsQty']]['answer'] : null;
		$pineSol = (isset($answers[$this->fieldsMapping['pineSol']]['answer']))? $answers[$this->fieldsMapping['pineSol']]['answer'] : null;
		$pineSolQty = (isset($answers[$this->fieldsMapping['pineSolQty']]['answer']))? $answers[$this->fieldsMapping['pineSolQty']]['answer'] : null;
		$toiletSeat = (isset($answers[$this->fieldsMapping['toiletSeat']]['answer']))? $answers[$this->fieldsMapping['toiletSeat']]['answer'] : null;
		$toiletSeatQty = (isset($answers[$this->fieldsMapping['toiletSeatQty']]['answer']))? $answers[$this->fieldsMapping['toiletSeatQty']]['answer'] : null;
		$toiletTissue = (isset($answers[$this->fieldsMapping['toiletTissue']]['answer']))? $answers[$this->fieldsMapping['toiletTissue']]['answer'] : null;
		$toiletTissueQty = (isset($answers[$this->fieldsMapping['toiletTissueQty']]['answer']))? $answers[$this->fieldsMapping['toiletTissueQty']]['answer'] : null;
		$trashBags33 = (isset($answers[$this->fieldsMapping['trashBags33']]['answer']))? $answers[$this->fieldsMapping['trashBags33']]['answer'] : null;
		$trashBags33Qty = (isset($answers[$this->fieldsMapping['trashBags33Qty']]['answer']))? $answers[$this->fieldsMapping['trashBags33Qty']]['answer'] : null;
		$trashBags13 = (isset($answers[$this->fieldsMapping['trashBags13']]['answer']))? $answers[$this->fieldsMapping['trashBags13']]['answer'] : null;
		$trashBags13Qty = (isset($answers[$this->fieldsMapping['trashBags13Qty']]['answer']))? $answers[$this->fieldsMapping['trashBags13Qty']]['answer'] : null;
		$windex = (isset($answers[$this->fieldsMapping['windex']]['answer']))? $answers[$this->fieldsMapping['windex']]['answer'] : null;
		$windexQty = (isset($answers[$this->fieldsMapping['windexQty']]['answer']))? $answers[$this->fieldsMapping['windexQty']]['answer'] : null;
		$jOther = (isset($answers[$this->fieldsMapping['jOther']]['answer']))? $answers[$this->fieldsMapping['jOther']]['answer'] : null;
		$jComments = (isset($answers[$this->fieldsMapping['jComments']]['answer']))? $answers[$this->fieldsMapping['jComments']]['answer'] : null;
		$showJanitorial = ($airFreshener || $bathroomCleaner || $carpetCleaner || $handSoap || $handSanitizer || $mopHead || $mopHandle || $paperTowels ||
			$pineSol || $toiletSeat || $toiletTissue || $trashBags33 || $trashBags13 || $windex || $jOther || $jComments);
		// Backpack tags
		$bpt999 = (isset($answers[$this->fieldsMapping['bpt999']]['answer']))? $answers[$this->fieldsMapping['bpt999']]['answer'] : null;
		$bpt999Qty = (isset($answers[$this->fieldsMapping['bpt999Qty']]['answer']))? $answers[$this->fieldsMapping['bpt999Qty']]['answer'] : null;
		$bpt2999 = (isset($answers[$this->fieldsMapping['bpt2999']]['answer']))? $answers[$this->fieldsMapping['bpt2999']]['answer'] : null;
		$bpt2999Qty = (isset($answers[$this->fieldsMapping['bpt2999Qty']]['answer']))? $answers[$this->fieldsMapping['bpt2999Qty']]['answer'] : null;
		$bptComments = (isset($answers[$this->fieldsMapping['bptComments']]['answer']))? $answers[$this->fieldsMapping['bptComments']]['answer'] : null;
		$showBackpackTags = ($bpt999 || $bpt2999 || $bptComments);
		// Sale Tags
		$percOffUgg = (isset($answers[$this->fieldsMapping['percOffUgg']]['answer']))? $answers[$this->fieldsMapping['percOffUgg']]['answer'] : null;
		$percOffUggQty = (isset($answers[$this->fieldsMapping['percOffUggQty']]['answer']))? $answers[$this->fieldsMapping['percOffUggQty']]['answer'] : null;
		$st499 = (isset($answers[$this->fieldsMapping['st499']]['answer']))? $answers[$this->fieldsMapping['st499']]['answer'] : null;
		$st499Qty = (isset($answers[$this->fieldsMapping['st499Qty']]['answer']))? $answers[$this->fieldsMapping['st499Qty']]['answer'] : null;
		$st5499 = (isset($answers[$this->fieldsMapping['st5499']]['answer']))? $answers[$this->fieldsMapping['st5499']]['answer'] : null;
		$st5499Qty = (isset($answers[$this->fieldsMapping['st5499Qty']]['answer']))? $answers[$this->fieldsMapping['st5499Qty']]['answer'] : null;
		$blank = (isset($answers[$this->fieldsMapping['blank']]['answer']))? $answers[$this->fieldsMapping['blank']]['answer'] : null;
		$blankQty = (isset($answers[$this->fieldsMapping['blankQty']]['answer']))? $answers[$this->fieldsMapping['blankQty']]['answer'] : null;
		$outletValue = (isset($answers[$this->fieldsMapping['outletValue']]['answer']))? $answers[$this->fieldsMapping['outletValue']]['answer'] : null;
		$outletValueQty = (isset($answers[$this->fieldsMapping['outletValueQty']]['answer']))? $answers[$this->fieldsMapping['outletValueQty']]['answer'] : null;
		$plasticTag = (isset($answers[$this->fieldsMapping['plasticTag']]['answer']))? $answers[$this->fieldsMapping['plasticTag']]['answer'] : null;
		$plasticTagQty = (isset($answers[$this->fieldsMapping['plasticTagQty']]['answer']))? $answers[$this->fieldsMapping['plasticTagQty']]['answer'] : null;
		$taggingGun = (isset($answers[$this->fieldsMapping['taggingGun']]['answer']))? $answers[$this->fieldsMapping['taggingGun']]['answer'] : null;
		$taggingGunQty = (isset($answers[$this->fieldsMapping['taggingGunQty']]['answer']))? $answers[$this->fieldsMapping['taggingGunQty']]['answer'] : null;
		$gunNeedles = (isset($answers[$this->fieldsMapping['gunNeedles']]['answer']))? $answers[$this->fieldsMapping['gunNeedles']]['answer'] : null;
		$gunNeedlesQty = (isset($answers[$this->fieldsMapping['gunNeedlesQty']]['answer']))? $answers[$this->fieldsMapping['gunNeedlesQty']]['answer'] : null;
		$priceGun = (isset($answers[$this->fieldsMapping['priceGun']]['answer']))? $answers[$this->fieldsMapping['priceGun']]['answer'] : null;
		$priceGunQty = (isset($answers[$this->fieldsMapping['priceGunQty']]['answer']))? $answers[$this->fieldsMapping['priceGunQty']]['answer'] : null;
		$stOther = (isset($answers[$this->fieldsMapping['stOther']]['answer']))? $answers[$this->fieldsMapping['stOther']]['answer'] : null;
		$stComments = (isset($answers[$this->fieldsMapping['stComments']]['answer']))? $answers[$this->fieldsMapping['stComments']]['answer'] : null;
		$showSaleTags = ($st499 || $st5499 || $blank || $outletValue || $plasticTag || $taggingGun || $gunNeedles || $priceGun || $stOther || $stComments);
		// Sale Stickers
		$blankPrice = (isset($answers[$this->fieldsMapping['blankPrice']]['answer']))? $answers[$this->fieldsMapping['blankPrice']]['answer'] : null;
		$blankPriceQty = (isset($answers[$this->fieldsMapping['blankPriceQty']]['answer']))? $answers[$this->fieldsMapping['blankPriceQty']]['answer'] : null;
		$stcOther = (isset($answers[$this->fieldsMapping['stcOther']]['answer']))? $answers[$this->fieldsMapping['stcOther']]['answer'] : null;
		$stcComments = (isset($answers[$this->fieldsMapping['stcComments']]['answer']))? $answers[$this->fieldsMapping['stcComments']]['answer'] : null;
		$showSaleStickers = ($blankPrice || $stcOther || $stcComments);
		// Shoeteria
		$sho999 = (isset($answers[$this->fieldsMapping['sho999']]['answer']))? $answers[$this->fieldsMapping['sho999']]['answer'] : null;
		$sho999Qty = (isset($answers[$this->fieldsMapping['sho999Qty']]['answer']))? $answers[$this->fieldsMapping['sho999Qty']]['answer'] : null;
		$sho1999 = (isset($answers[$this->fieldsMapping['sho1999']]['answer']))? $answers[$this->fieldsMapping['sho1999']]['answer'] : null;
		$sho1999Qty = (isset($answers[$this->fieldsMapping['sho1999Qty']]['answer']))? $answers[$this->fieldsMapping['sho1999Qty']]['answer'] : null;
		$sho2999 = (isset($answers[$this->fieldsMapping['sho2999']]['answer']))? $answers[$this->fieldsMapping['sho2999']]['answer'] : null;
		$sho2999Qty = (isset($answers[$this->fieldsMapping['sho2999Qty']]['answer']))? $answers[$this->fieldsMapping['sho2999Qty']]['answer'] : null;
		$sho3999 = (isset($answers[$this->fieldsMapping['sho3999']]['answer']))? $answers[$this->fieldsMapping['sho3999']]['answer'] : null;
		$sho3999Qty = (isset($answers[$this->fieldsMapping['sho3999Qty']]['answer']))? $answers[$this->fieldsMapping['sho3999Qty']]['answer'] : null;
		$sho4999 = (isset($answers[$this->fieldsMapping['sho4999']]['answer']))? $answers[$this->fieldsMapping['sho4999']]['answer'] : null;
		$sho4999Qty = (isset($answers[$this->fieldsMapping['sho4999Qty']]['answer']))? $answers[$this->fieldsMapping['sho4999Qty']]['answer'] : null;
		$shoBlank = (isset($answers[$this->fieldsMapping['shoBlank']]['answer']))? $answers[$this->fieldsMapping['shoBlank']]['answer'] : null;
		$shoBlankQty = (isset($answers[$this->fieldsMapping['shoBlankQty']]['answer']))? $answers[$this->fieldsMapping['shoBlankQty']]['answer'] : null;
		$shoComments = (isset($answers[$this->fieldsMapping['shoComments']]['answer']))? $answers[$this->fieldsMapping['shoComments']]['answer'] : null;
		$showShoteria = ($sho999 || $sho1999 || $sho2999 || $sho3999 || $sho4999 || $shoBlank || $shoComments);
		// Comments / Special Request
		$specialComment = (isset($answers[$this->fieldsMapping['specialComment']]['answer']))? $answers[$this->fieldsMapping['specialComment']]['answer'] : null;
		$showSpecialRequest = ($specialComment);
		// Reference #
		$reference = (isset($answers[$this->fieldsMapping['reference']]['answer']))? $answers[$this->fieldsMapping['reference']]['answer'] : null;

		// HTML Body
		$body = '<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f7f9fc">';
		$body .= '<tbody><tr><td height="30">&nbsp;';
		$body .= '<p style="text-align: center;"><span style="font-size: large;"><strong>SUPPLY REQUISITION</strong></span></p>';
		$body .= '<p style="text-align: center;">&nbsp;</p></td></tr>';
		$body .= '<tr><td align="center"><table border="0" cellspacing="0" cellpadding="0" width="600" bgcolor="#eeeeee">';
		$body .= '<tbody><tr>';
		$body .= '<td width="13" height="30" background="http://www.jotform.com/images/win2_title_left.gif">&nbsp;</td>';
		$body .= '<td align="left" valign="bottom" background="http://www.jotform.com/images/win2_title.gif"><br /></td>';
		$body .= '<td width="14" background="http://www.jotform.com/images/win2_title_right.gif">&nbsp;</td>';
		$body .= '</tr></tbody></table>';
		$body .= '<table border="0" cellspacing="0" cellpadding="0" width="600" bgcolor="#eeeeee">';
		$body .= '<tbody><tr><td width="4" background="http://www.jotform.com/images/win2_left.gif">&nbsp;</td>';
		$body .= '<td align="center" bgcolor="#FFFFFF">';
		$body .= '<table id="emailFieldsTable" class="mceNonEditable" border="0" cellspacing="0" cellpadding="5" width="100%">';
		$body .= '<tbody><tr>';
		$body .= '<td style="text-decoration: underline; padding: 5px !important;" width="170" bgcolor="#f9f9f9"><strong>Question</strong></td>';
		$body .= '<td style="text-decoration: underline; padding: 5px !important;" bgcolor="#f9f9f9" colspan="2"><strong>Answer</strong></td></tr>';

		$bodyText = 'SUPPLY REQUISITION \n';
		$bodyText .= '----------------------------------- \n';

		// Store
		$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="white">Store</td>';
		$body .= '<td style="padding: 5px !important;" bgcolor="white" colspan="2">' . $store . '</td></tr>';
		// Manager
		$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="#f9f9f9">Area/District Manager</td>';
		$body .= '<td style="padding: 5px !important;" bgcolor="#f9f9f9" colspan="2">' . $manager . '</td></tr>';
		// Requested By
		$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="white">Requested by</td>';
		$body .= '<td style="padding: 5px !important;" bgcolor="white" colspan="2">' . $requestedBy . '</td></tr>';

		$bodyText .= 'Store: ' . $store . ' \n';
		$bodyText .= 'Area/District Manager: ' . $manager . ' \n';
		$bodyText .= 'Requested by: ' . $requestedBy . ' \n';

		// Human Resources
		if ($showHumanResources) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>HUMAN RESOURCES</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'HUMAN RESOURCES \n';
			$bodyText .= '----------------------------------- \n';

			// Medical Application
			if (!is_null($medicalApp)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Medical Ins. Application</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $medicalApp . '</td></tr>';
				$bodyText .= 'Medical Ins. Application: ' . $medicalApp . '\n';
				$flag = !$flag;
			}
			// Medical Application Quantity
			if (!is_null($medicalAppQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $medicalAppQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $medicalAppQty . '\n';
			}
			// Dental Application
			if (!is_null($dentalApp)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Dental/Vision Ins. Application</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $dentalApp . '</td></tr>';
				$bodyText .= 'Dental/Vision Ins. Application: ' . $dentalApp . '\n';
				$flag = !$flag;
			}
			// Dental Application Quantity
			if (!is_null($dentalAppQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $dentalAppQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $dentalAppQty . '\n';
			}
			// Workers parkers
			if (!is_null($workersPacket)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Workers Comp. Packet</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $workersPacket . '</td></tr>';
				$bodyText .= 'Workers Comp. Packet: ' . $workersPacket . '\n';
				$flag = !$flag;
			}
			// Workers Packet Quantity
			if (!is_null($workersPacketQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $workersPacketQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $workersPacketQty . '\n';
			}
			// Policies
			if (!is_null($policies)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Policies And Procedures Book</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $policies . '</td></tr>';
				$bodyText .= 'Policies And Procedures Book: ' . $policies . '\n';
				$flag = !$flag;
			}
			// Policies Quantity
			if (!is_null($policiesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $policiesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $policiesQty . '\n';
			}
			// Tick Binder
			if (!is_null($tick)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Tick Binder</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $tick . '</td></tr>';
				$bodyText .= 'Tick Binder: ' . $tick . '\n';
				$flag = !$flag;
			}
			// Tick Binder Quantity
			if (!is_null($tickQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $tickQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $tickQty . '\n';
			}
			// Other
			if (!is_null($rhOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $rhOther . '</td></tr>';
				$bodyText .= 'Other: ' . $rhOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($rhComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $rhComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $rhComments . '\n';
				$flag = !$flag;
			}
		}
		// Operation Forms
		if ($showOperationForms) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>OPERATION FORMS</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'OPERATION FORMS \n';
			$bodyText .= '----------------------------------- \n';

			// Business Cards
			if (!is_null($businessCards)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Business Cards</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $businessCards . '</td></tr>';
				$bodyText .= 'Business Cards: ' . $businessCards . '\n';
				$flag = !$flag;
			}
			// Business Cards Quantity
			if (!is_null($businessCardsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $businessCardsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $businessCardsQty . '\n';
			}
			// CC Imprint
			if (!is_null($ccImprint)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">C.C. Imprint Receipt</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $ccImprint . '</td></tr>';
				$bodyText .= 'C.C. Imprint Receipt: ' . $ccImprint . '\n';
				$flag = !$flag;
			}
			// CC Imprint Quantity
			if (!is_null($ccImprintQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $ccImprintQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $ccImprintQty . '\n';
			}
			// CC Terminal
			if (!is_null($ccTerminal)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">C.C. Terminal Receipt Paper</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $ccTerminal . '</td></tr>';
				$bodyText .= 'C.C. Terminal Receipt Paper: ' . $ccTerminal . '\n';
				$flag = !$flag;
			}
			// CC Terminal Quantity
			if (!is_null($ccTerminalQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $ccTerminalQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $ccTerminalQty . '\n';
			}
			// Register
			if (!is_null($register)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Register Receipt Paper Roll</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $register . '</td></tr>';
				$bodyText .= 'Register Receipt Paper Roll: ' . $register . '\n';
				$flag = !$flag;
			}
			// Register Quantity
			if (!is_null($registerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $registerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $registerQty . '\n';
			}
			// Deposit Bags
			if (!is_null($depositBags)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Deposit Bags</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $depositBags . '</td></tr>';
				$bodyText .= 'Deposit Bags: ' . $depositBags . '\n';
				$flag = !$flag;
			}
			// Deposit Bags Quantity
			if (!is_null($depositBagsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $depositBagsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $depositBagsQty . '\n';
			}
			// Deposit Slips
			if (!is_null($depositSlips)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Deposit Slips</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $depositSlips . '</td></tr>';
				$bodyText .= 'Deposit Slips: ' . $depositSlips . '\n';
				$flag = !$flag;
			}
			// Deposit Slips Quantity
			if (!is_null($depositSlipsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $depositSlipsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $depositSlipsQty . '\n';
			}
			// Manual Receipts
			if (!is_null($manual)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Manual Receipts</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $manual . '</td></tr>';
				$bodyText .= 'Manual Receipts: ' . $manual . '\n';
				$flag = !$flag;
			}
			// Manual Receipts Quantity
			if (!is_null($manualQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $manualQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $manualQty . '\n';
			}
			// Stickers
			if (!is_null($stickers)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Stickers (Transfer-Out)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stickers . '</td></tr>';
				$bodyText .= 'Stickers (Transfer-Out): ' . $stickers . '\n';
				$flag = !$flag;
			}
			// Stickers Quantity
			if (!is_null($stickersQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stickersQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $stickersQty . '\n';
			}
			// Comments
			if (!is_null($ofComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $ofComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $ofComments . '\n';
				$flag = !$flag;
			}
		}
		// Light Bulbs
		if ($showLightBulbs) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>LIGHT BULBS</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'LIGHT BULBS \n';
			$bodyText .= '----------------------------------- \n';

			// Sales Floor
			if (!is_null($salesFloor)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Sales-floor</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $salesFloor . '</td></tr>';
				$bodyText .= 'Sales-floor: ' . $salesFloor . '\n';
				$flag = !$flag;
			}
			// Light Bulb model
			if (!is_null($lbModel)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Light Bulb Model</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $lbModel . '</td></tr>';
				$bodyText .= 'Light Bulb Model: ' . $lbModel . '\n';
			}
			// Sales Floor Quantity
			if (!is_null($salesFloorQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $salesFloorQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $salesFloorQty . '\n';
			}
			// Stockroom
			if (!is_null($stockroom)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Stockroom</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stockroom . '</td></tr>';
				$bodyText .= 'Stockroom: ' . $stockroom . '\n';
				$flag = !$flag;
			}
			// Stockroom light bulb model
			if (!is_null($slbModel)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Light Bulb Model</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $slbModel . '</td></tr>';
				$bodyText .= 'Light Bulb Model: ' . $slbModel . '\n';
			}
			// StockRoom Quantity
			if (!is_null($stockroomQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stockroomQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $stockroomQty . '\n';
			}
			// Other
			if (!is_null($lbOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $lbOther . '</td></tr>';
				$bodyText .= 'Other: ' . $lbOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($lbComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $lbComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $lbComments . '\n';
				$flag = !$flag;
			}
		}
		// Supplies
		if ($showSupplies) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>SUPPLIES</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'SUPPLIES \n';
			$bodyText .= '----------------------------------- \n';

			// Box Cutter: Blades
			if (!is_null($boxCutterBlades)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Box Cutter: Blades</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $boxCutterBlades . '</td></tr>';
				$bodyText .= 'Box Cutter: Blades: ' . $boxCutterBlades . '\n';
				$flag = !$flag;
			}
			// Box Cutter: Blades Quantity
			if (!is_null($boxCutterBladesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $boxCutterBladesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $boxCutterBladesQty . '\n';
			}
			// Box Cutter: Handles
			if (!is_null($boxCutterHandles)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Box Cutter: Handles</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $boxCutterHandles . '</td></tr>';
				$bodyText .= 'Box Cutter: Handles: ' . $boxCutterHandles . '\n';
				$flag = !$flag;
			}
			// Box Cutter: Handles Quantity
			if (!is_null($boxCutterHandlesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $boxCutterHandlesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $boxCutterHandlesQty . '\n';
			}
			// Calculator
			if (!is_null($calculator)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Calculator</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $calculator . '</td></tr>';
				$bodyText .= 'Calculator: ' . $calculator . '\n';
				$flag = !$flag;
			}
			// Calculator Quantity
			if (!is_null($calculatorQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $calculatorQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $calculatorQty . '\n';
			}
			// Envelopes
			if (!is_null($envelopes)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Envelopes</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $envelopes . '</td></tr>';
				$bodyText .= 'Envelopes: ' . $envelopes . '\n';
				$flag = !$flag;
			}
			// Envelopes Quantity
			if (!is_null($envelopesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $envelopesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $envelopesQty . '\n';
			}
			// Envelopes 9 x 12
			if (!is_null($envelopes912)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Envelopes 9 x 12</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $envelopes912 . '</td></tr>';
				$bodyText .= 'Envelopes 9 x 12: ' . $envelopes912 . '\n';
				$flag = !$flag;
			}
			// Envelopes 9 x 12 Quantity
			if (!is_null($envelopes912Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $envelopes912Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $envelopes912Qty . '\n';
			}
			// Grey Bags (for paperwork)
			if (!is_null($greyBags)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Grey Bags (for paperwork)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $greyBags . '</td></tr>';
				$bodyText .= 'Grey Bags (for paperwork): ' . $greyBags . '\n';
				$flag = !$flag;
			}
			// Grey Bags (for paperwork) Quantity
			if (!is_null($greyBagsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $greyBagsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $greyBagsQty . '\n';
			}
			// Highliter
			if (!is_null($highliter)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Highliter</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $highliter . '</td></tr>';
				$bodyText .= 'Highliter: ' . $highliter . '\n';
				$flag = !$flag;
			}
			// Highliter Quantity
			if (!is_null($highliterQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $highliterQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $highliterQty . '\n';
			}
			// Markers (Jumbo)
			if (!is_null($markersJumbo)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Markers (Jumbo)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $markersJumbo . '</td></tr>';
				$bodyText .= 'Markers (Jumbo): ' . $markersJumbo . '\n';
				$flag = !$flag;
			}
			// Markers (Jumbo) Quantity
			if (!is_null($markersJumboQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $markersJumboQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $markersJumboQty . '\n';
			}
			// Markers (Sharpie)
			if (!is_null($markersSharpie)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Markers (Sharpie)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $markersSharpie . '</td></tr>';
				$bodyText .= 'Markers (Sharpie): ' . $markersSharpie . '\n';
				$flag = !$flag;
			}
			// Markers (Sharpie) Quantity
			if (!is_null($markersSharpieQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $markersSharpieQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $markersSharpieQty . '\n';
			}
			// Notebooks
			if (!is_null($notebooks)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Notebooks</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $notebooks . '</td></tr>';
				$bodyText .= 'Notebooks: ' . $notebooks . '\n';
				$flag = !$flag;
			}
			// Notebooks Quantity
			if (!is_null($notebooksQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $notebooksQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $notebooksQty . '\n';
			}
			// Packing List Envelopes
			if (!is_null($packingEnvelopes)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Packing List Envelopes</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $packingEnvelopes . '</td></tr>';
				$bodyText .= 'Packing List Envelopes: ' . $packingEnvelopes . '\n';
				$flag = !$flag;
			}
			// Packing List Envelopes Quantity
			if (!is_null($packingEnvelopesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $packingEnvelopesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $packingEnvelopesQty . '\n';
			}
			// Packing Tape
			if (!is_null($packingTape)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Packing Tape</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $packingTape . '</td></tr>';
				$bodyText .= 'Packing Tape: ' . $packingTape . '\n';
				$flag = !$flag;
			}
			// Packing Tape Quantity
			if (!is_null($packingTapeQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $packingTapeQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $packingTapeQty . '\n';
			}
			// Paper (Printer & Fax)
			if (!is_null($paperPrinter)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Paper (Printer & Fax)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperPrinter . '</td></tr>';
				$bodyText .= 'Paper (Printer & Fax): ' . $paperPrinter . '\n';
				$flag = !$flag;
			}
			// Paper (Printer & Fax) Quantity
			if (!is_null($paperPrinterQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperPrinterQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $paperPrinterQty . '\n';
			}
			// Paper Clips
			if (!is_null($paperClips)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Paper Clips</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperClips . '</td></tr>';
				$bodyText .= 'Paper Clips: ' . $paperClips . '\n';
				$flag = !$flag;
			}
			// Paper Clips Quantity
			if (!is_null($paperClipsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperClipsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $paperClipsQty . '\n';
			}
			// Pens
			if (!is_null($pens)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Pens</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $pens . '</td></tr>';
				$bodyText .= 'Pens: ' . $pens . '\n';
				$flag = !$flag;
			}
			// Pens Quantity
			if (!is_null($pensQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $pensQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $pensQty . '\n';
			}
			// Rubber Bands
			if (!is_null($rubberBands)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Rubber Bands</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $rubberBands . '</td></tr>';
				$bodyText .= 'Rubber Bands: ' . $rubberBands . '\n';
				$flag = !$flag;
			}
			// Rubber Bands Quantity
			if (!is_null($rubberBandsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $rubberBandsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $rubberBandsQty . '\n';
			}
			// Scissors
			if (!is_null($scissors)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Scissors</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $scissors . '</td></tr>';
				$bodyText .= 'Scissors: ' . $scissors . '\n';
				$flag = !$flag;
			}
			// Scissors Quantity
			if (!is_null($scissorsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $scissorsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $scissorsQty . '\n';
			}
			// Stapler
			if (!is_null($stapler)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Stapler</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stapler . '</td></tr>';
				$bodyText .= 'Stapler: ' . $stapler . '\n';
				$flag = !$flag;
			}
			// Stapler Quantity
			if (!is_null($staplerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $staplerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $staplerQty . '\n';
			}
			// Staples
			if (!is_null($staples)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Staples</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $staples . '</td></tr>';
				$bodyText .= 'Staples: ' . $staples . '\n';
				$flag = !$flag;
			}
			// Staples Quantity
			if (!is_null($staplesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $staplesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $staplesQty . '\n';
			}
			// Store Bags: Big
			if (!is_null($storeBagsBig)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Store Bags: Big</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $storeBagsBig . '</td></tr>';
				$bodyText .= 'Store Bags: Big: ' . $storeBagsBig . '\n';
				$flag = !$flag;
			}
			// Store Bags: Big Quantity
			if (!is_null($storeBagsBigQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $storeBagsBigQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $storeBagsBigQty . '\n';
			}
			// Store Bags: Small
			if (!is_null($storeBagsSmall)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Store Bags: Small</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $storeBagsSmall . '</td></tr>';
				$bodyText .= 'Store Bags: Small: ' . $storeBagsSmall . '\n';
				$flag = !$flag;
			}
			// Store Bags: Small Quantity
			if (!is_null($storeBagsSmallQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $storeBagsSmallQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $storeBagsSmallQty . '\n';
			}
			// Store Policy Sign
			if (!is_null($policySign)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Store Policy Sign</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $policySign . '</td></tr>';
				$bodyText .= 'Store Policy Sign: ' . $policySign . '\n';
				$flag = !$flag;
			}
			// Store Policy Sign Quantity
			if (!is_null($policySignQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $policySignQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $policySignQty . '\n';
			}
			// Scotch Tape (Clear Tape)
			if (!is_null($scotchTape)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Scotch Tape (Clear Tape)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $scotchTape . '</td></tr>';
				$bodyText .= 'Scotch Tape (Clear Tape): ' . $scotchTape . '\n';
				$flag = !$flag;
			}
			// Scotch Tape (Clear Tape) Quantity
			if (!is_null($scotchTapeQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $scotchTapeQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $scotchTapeQty . '\n';
			}
			// Toner (Printer)
			if (!is_null($toner)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Toner (Printer)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $toner . '</td></tr>';
				$bodyText .= 'Toner (Printer): ' . $toner . '\n';
				$flag = !$flag;
			}
			// Toner Printer Model
			if (!is_null($tonerPrinterModel)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Printer Model</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $tonerPrinterModel . '</td></tr>';
				$bodyText .= 'Printer Model: ' . $tonerPrinterModel . '\n';
			}
			// Toner (Printer) Quantity
			if (!is_null($tonerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $tonerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $tonerQty . '\n';
			}
			// White Out Tape
			if (!is_null($whiteOut)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">White Out Tape</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $whiteOut . '</td></tr>';
				$bodyText .= 'White Out Tape: ' . $whiteOut . '\n';
				$flag = !$flag;
			}
			// White Out Tape Quantity
			if (!is_null($whiteOutQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $whiteOutQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $whiteOutQty . '\n';
			}
			// Try on Socks (Nylon)
			if (!is_null($trySocks)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Try on Socks (Nylon)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trySocks . '</td></tr>';
				$bodyText .= 'Try on Socks (Nylon): ' . $trySocks . '\n';
				$flag = !$flag;
			}
			// Try on Socks (Nylon) Quantity
			if (!is_null($trySocksQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trySocksQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $trySocksQty . '\n';
			}
			// Other
			if (!is_null($sOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sOther . '</td></tr>';
				$bodyText .= 'Other: ' . $sOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($sComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $sComments . '\n';
				$flag = !$flag;
			}
		}
		// JANITORIAL (DROP SHIPMENT)
		if ($showJanitorial) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>JANITORIAL (DROP SHIPMENT)</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'JANITORIAL (DROP SHIPMENT) \n';
			$bodyText .= '----------------------------------- \n';

			// Air Freshener
			if (!is_null($airFreshener)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Air Freshener</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $airFreshener . '</td></tr>';
				$bodyText .= 'Air Freshener: ' . $airFreshener . '\n';
				$flag = !$flag;
			}
			// Air Freshener Quantity
			if (!is_null($airFreshenerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $airFreshenerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $airFreshenerQty . '\n';
			}
			// Bathroom Cleaner
			if (!is_null($bathroomCleaner)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Bathroom Cleaner</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bathroomCleaner . '</td></tr>';
				$bodyText .= 'Bathroom Cleaner: ' . $bathroomCleaner . '\n';
				$flag = !$flag;
			}
			// Bathroom Cleaner Quantity
			if (!is_null($bathroomCleanerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bathroomCleanerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $bathroomCleanerQty . '\n';
			}
			// Carpet Cleaner
			if (!is_null($carpetCleaner)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Carpet Cleaner</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $carpetCleaner . '</td></tr>';
				$bodyText .= 'Carpet Cleaner: ' . $carpetCleaner . '\n';
				$flag = !$flag;
			}
			// Carpet Cleaner Quantity
			if (!is_null($carpetCleanerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $carpetCleanerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $carpetCleanerQty . '\n';
			}
			// Hand Soap
			if (!is_null($handSoap)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Hand Soap</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $handSoap . '</td></tr>';
				$bodyText .= 'Hand Soap: ' . $handSoap . '\n';
				$flag = !$flag;
			}
			// Hand Soap Quantity
			if (!is_null($handSoapQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $handSoapQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $handSoapQty . '\n';
			}
			// Hand Sanitizer
			if (!is_null($handSanitizer)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Hand Sanitizer</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $handSanitizer . '</td></tr>';
				$bodyText .= 'Hand Sanitizer: ' . $handSanitizer . '\n';
				$flag = !$flag;
			}
			// Hand Sanitizer Quantity
			if (!is_null($handSanitizerQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $handSanitizerQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $handSanitizerQty . '\n';
			}
			// Mop: Head
			if (!is_null($mopHead)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Mop: Head</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $mopHead . '</td></tr>';
				$bodyText .= 'Mop: Head: ' . $mopHead . '\n';
				$flag = !$flag;
			}
			// Mop: Head Quantity
			if (!is_null($mopHeadQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $mopHeadQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $mopHeadQty . '\n';
			}
			// Mop: Handle
			if (!is_null($mopHandle)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Mop: Handle</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $mopHandle . '</td></tr>';
				$bodyText .= 'Mop: Handle: ' . $mopHandle . '\n';
				$flag = !$flag;
			}
			// Mop: Handle Quantity
			if (!is_null($mopHandleQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $mopHandleQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $mopHandleQty . '\n';
			}
			// Paper Towels
			if (!is_null($paperTowels)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Paper Towels</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperTowels . '</td></tr>';
				$bodyText .= 'Paper Towels: ' . $paperTowels . '\n';
				$flag = !$flag;
			}
			// Paper Towels Quantity
			if (!is_null($paperTowelsQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $paperTowelsQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $paperTowelsQty . '\n';
			}
			// Pine Sol
			if (!is_null($pineSol)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Pine Sol</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $pineSol . '</td></tr>';
				$bodyText .= 'Pine Sol: ' . $pineSol . '\n';
				$flag = !$flag;
			}
			// Pine Sol Quantity
			if (!is_null($pineSolQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $pineSolQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $pineSolQty . '\n';
			}
			// Toilet Seat Cover
			if (!is_null($toiletSeat)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Toilet Seat Cover</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $toiletSeat . '</td></tr>';
				$bodyText .= 'Toilet Seat Cover: ' . $toiletSeat . '\n';
				$flag = !$flag;
			}
			// Toilet Seat Cover Quantity
			if (!is_null($toiletSeatQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $toiletSeatQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $toiletSeatQty . '\n';
			}
			// Toilet Tissue
			if (!is_null($toiletTissue)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Toilet Tissue</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $toiletTissue . '</td></tr>';
				$bodyText .= 'Toilet Tissue: ' . $toiletTissue . '\n';
				$flag = !$flag;
			}
			// Toilet Tissue Quantity
			if (!is_null($toiletTissueQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $toiletTissueQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $toiletTissueQty . '\n';
			}
			// Trash Bags (White 33gl)
			if (!is_null($trashBags33)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Trash Bags (White 33gl)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trashBags33 . '</td></tr>';
				$bodyText .= 'Trash Bags (White 33gl): ' . $trashBags33 . '\n';
				$flag = !$flag;
			}
			// Trash Bags (White 33gl) Quantity
			if (!is_null($trashBags33Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trashBags33Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $trashBags33Qty . '\n';
			}
			// Trash Bags (White 13gl)
			if (!is_null($trashBags13)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Trash Bags (White 13gl)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trashBags13 . '</td></tr>';
				$bodyText .= 'Trash Bags (White 13gl): ' . $trashBags13 . '\n';
				$flag = !$flag;
			}
			// Trash Bags (White 13gl) Quantity
			if (!is_null($trashBags13Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $trashBags13Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $trashBags13Qty . '\n';
			}
			// Windex
			if (!is_null($windex)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Windex</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $windex . '</td></tr>';
				$bodyText .= 'Windex: ' . $windex . '\n';
				$flag = !$flag;
			}
			// Windex Quantity
			if (!is_null($windexQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $windexQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $windexQty . '\n';
			}
			// Other
			if (!is_null($jOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $jOther . '</td></tr>';
				$bodyText .= 'Other: ' . $jOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($jComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $jComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $jComments . '\n';
				$flag = !$flag;
			}
		}
		// Backpack Tags
		if ($showBackpackTags) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>BACKPACK TAGS</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'BACKPACK TAGS \n';
			$bodyText .= '----------------------------------- \n';

			// $9.99 - $19.99
			if (!is_null($bpt999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$9.99 - $19.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bpt999 . '</td></tr>';
				$bodyText .= '$9.99 - $19.99: ' . $bpt999 . '\n';
				$flag = !$flag;
			}
			// $9.99 - $19.99 Quantity
			if (!is_null($bpt999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bpt999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $bpt999Qty . '\n';
			}
			// $29.99
			if (!is_null($bpt2999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$29.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bpt2999 . '</td></tr>';
				$bodyText .= '$29.99: ' . $bpt2999 . '\n';
				$flag = !$flag;
			}
			// $29.99 Quantity
			if (!is_null($bpt2999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bpt2999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $bpt2999Qty . '\n';
			}
			// Comments
			if (!is_null($bptComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $bptComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $bptComments . '\n';
				$flag = !$flag;
			}
		}
		// Sale Tags
		if ($showSaleTags) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>SALE TAGS</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'SALE TAGS \n';
			$bodyText .= '----------------------------------- \n';

			// Percentage Off UGG
			if (!is_null($percOffUgg)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Percentage Off (UGG & Timberland Only)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $percOffUgg . '</td></tr>';
				$bodyText .= 'Percentage Off (UGG & Timberland Only): ' . $percOffUgg . '\n';
				$flag = !$flag;
			}
			// Percentage Off UGG Quantity
			if (!is_null($percOffUggQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $percOffUggQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $percOffUggQty . '\n';
			}
			// $4.99 - $49.99
			if (!is_null($st499)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$4.99 - $49.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $st499 . '</td></tr>';
				$bodyText .= '$4.99 - $49.99: ' . $st499 . '\n';
				$flag = !$flag;
			}
			// $4.99 - $49.99 Quantity
			if (!is_null($st499Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $st499Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $st499Qty . '\n';
			}
			// $54.99 - $129.99
			if (!is_null($st5499)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$54.99 - $129.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $st5499 . '</td></tr>';
				$bodyText .= '54.99 - $129.99: ' . $st5499 . '\n';
				$flag = !$flag;
			}
			// $54.99 - $129.99 Quantity
			if (!is_null($st5499Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $st5499Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $st5499Qty . '\n';
			}
			// Blank
			if (!is_null($blank)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Blank</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $blank . '</td></tr>';
				$bodyText .= 'Blank: ' . $blank . '\n';
				$flag = !$flag;
			}
			// Blank Quantity
			if (!is_null($blankQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $blankQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $blankQty . '\n';
			}
			// Outlet Value (Outlet Stores Only)
			if (!is_null($outletValue)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Outlet Value (Outlet Stores Only)</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $outletValue . '</td></tr>';
				$bodyText .= 'Outlet Value (Outlet Stores Only): ' . $outletValue . '\n';
				$flag = !$flag;
			}
			// Outlet Value (Outlet Stores Only) Quantity
			if (!is_null($outletValueQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $outletValueQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $outletValueQty . '\n';
			}
			// Plastic Tag Pins
			if (!is_null($plasticTag)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Plastic Tag Pins</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $plasticTag . '</td></tr>';
				$bodyText .= 'Plastic Tag Pins: ' . $plasticTag . '\n';
				$flag = !$flag;
			}
			// Plastic Tag Pins Quantity
			if (!is_null($plasticTagQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $plasticTagQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $plasticTagQty . '\n';
			}
			// Tagging Gun
			if (!is_null($taggingGun)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Tagging Gun</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $taggingGun . '</td></tr>';
				$bodyText .= 'Tagging Gun: ' . $taggingGun . '\n';
				$flag = !$flag;
			}
			// Tagging Gun Quantity
			if (!is_null($taggingGunQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $taggingGunQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $taggingGunQty . '\n';
			}
			// Tagging Gun Needles
			if (!is_null($gunNeedles)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Tagging Gun Needles</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $gunNeedles . '</td></tr>';
				$bodyText .= 'Tagging Gun Needles: ' . $gunNeedles . '\n';
				$flag = !$flag;
			}
			// Tagging Gun Needles Quantity
			if (!is_null($gunNeedlesQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $gunNeedlesQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $gunNeedlesQty . '\n';
			}
			// Price Gun
			if (!is_null($priceGun)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Price Gun</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $priceGun . '</td></tr>';
				$bodyText .= 'Price Gun: ' . $priceGun . '\n';
				$flag = !$flag;
			}
			// Price Gun Quantity
			if (!is_null($priceGunQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $priceGunQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $priceGunQty . '\n';
			}
			// Other
			if (!is_null($stOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stOther . '</td></tr>';
				$bodyText .= 'Other: ' . $stOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($stComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $stComments . '\n';
				$flag = !$flag;
			}
		}
		// Sale Stickers
		if ($showSaleStickers) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>SALE STICKERS</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'SALE STICKERS \n';
			$bodyText .= '----------------------------------- \n';

			// Blank Price Sticker Sheet
			if (!is_null($blankPrice)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Blank Price Sticker Sheet</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $blankPrice . '</td></tr>';
				$bodyText .= 'Blank Price Sticker Sheet: ' . $blankPrice . '\n';
				$flag = !$flag;
			}
			// Blank Price Sticker Sheet Quantity
			if (!is_null($blankPriceQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $blankPriceQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $blankPriceQty . '\n';
			}
			// Other
			if (!is_null($stcOther)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Other</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stcOther . '</td></tr>';
				$bodyText .= 'Other: ' . $stcOther . '\n';
				$flag = !$flag;
			}
			// Comments
			if (!is_null($stcComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $stcComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $stcComments . '\n';
				$flag = !$flag;
			}
		}
		// Shoteria
		if ($showShoteria) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>SHOETERIA SALE STICKERS (Orange)</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'SHOETERIA SALE STICKERS (Orange) \n';
			$bodyText .= '----------------------------------- \n';

			// $9.99
			if (!is_null($sho999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$9.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho999 . '</td></tr>';
				$bodyText .= '$9.99: ' . $sho999 . '\n';
				$flag = !$flag;
			}
			// $9.99 Quantity
			if (!is_null($sho999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $sho999Qty . '\n';
			}
			// $19.99
			if (!is_null($sho1999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$19.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho1999 . '</td></tr>';
				$bodyText .= '$19.99: ' . $sho1999 . '\n';
				$flag = !$flag;
			}
			// $19.99 Quantity
			if (!is_null($sho1999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho1999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $sho1999Qty . '\n';
			}
			// $29.99
			if (!is_null($sho2999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$29.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho2999 . '</td></tr>';
				$bodyText .= '$29.99: ' . $sho2999 . '\n';
				$flag = !$flag;
			}
			// $29.99 Quantity
			if (!is_null($sho2999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho2999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $sho2999Qty . '\n';
			}
			// $39.99
			if (!is_null($sho3999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$39.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho3999 . '</td></tr>';
				$bodyText .= '$39.99: ' . $sho3999 . '\n';
				$flag = !$flag;
			}
			// $39.99 Quantity
			if (!is_null($sho3999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho3999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $sho3999Qty . '\n';
			}
			// $49.99
			if (!is_null($sho4999)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">$49.99</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho4999 . '</td></tr>';
				$bodyText .= '$49.99: ' . $sho4999 . '\n';
				$flag = !$flag;
			}
			// $49.99 Quantity
			if (!is_null($sho4999Qty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $sho4999Qty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $sho4999Qty . '\n';
			}
			// Blank
			if (!is_null($shoBlank)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Blank</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $shoBlank . '</td></tr>';
				$bodyText .= 'Blank: ' . $shoBlank . '\n';
				$flag = !$flag;
			}
			// Blank Quantity
			if (!is_null($shoBlankQty)) {
				$body .= '<tr><td align="center" style="padding: 0px 5px 5px !important;" width="170" bgcolor="'.$rowColor.'">Quantity in Stock</td>';
				$body .= '<td style="padding: 0px 5px 5px 40px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $shoBlankQty . '</td></tr>';
				$bodyText .= 'Quantity in Stock: ' . $shoBlankQty . '\n';
			}
			// Comments
			if (!is_null($shoComments)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="'.$rowColor.'" colspan="2">' . $shoComments . '</td></tr>';
				$bodyText .= 'Comments: ' . $shoComments . '\n';
				$flag = !$flag;
			}
		}
		// Comments / Special Request
		if ($showSpecialRequest) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>COMMENTS / SPECIAL REQUEST</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= 'COMMENTS / SPECIAL REQUEST \n';
			$bodyText .= '----------------------------------- \n';

			// Comments
			if (!is_null($specialComment)) {
				$rowColor = $this->rowColors[$flag];
				$body .= '<tr><td style="padding: 5px !important;" width="170" bgcolor="'.$rowColor.'">Comments</td>';
				$body .= '<td style="padding: 5px !important;" bgcolor="#f9f9f9" colspan="2">' . $specialComment . '</td></tr>';
				$bodyText .= 'Comments: ' . $specialComment . '\n';
				$flag = !$flag;
			}
		}
		if ($reference) {
			//$body .= '<tr><td width="13" height="30" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td>';
			$body .= '<tr><td align="center" height="30" colspan="3" valign="middle" background="http://www.jotform.com/images/win2_title.gif"><strong>'.$reference.'</strong></td></tr>';
			//$body .= '<td width="14" background="http://www.jotform.com/images/win2_title.gif">&nbsp;</td></tr>';

			$bodyText .= $reference . ' \n';
			$bodyText .= '----------------------------------- \n';
		}
		$body .= '</tbody></table></td>';
		$body .= '<td width="4" background="http://www.jotform.com/images/win2_right.gif">&nbsp;</td></tr>';
		$body .= '<tr><td style="font-size: 4px;" height="4" background="http://www.jotform.com/images/win2_foot_left.gif">&nbsp;</td>';
		$body .= '<td style="font-size: 4px;" background="http://www.jotform.com/images/win2_foot.gif">&nbsp;</td>';
		$body .= '<td style="font-size: 4px;" background="http://www.jotform.com/images/win2_foot_right.gif">&nbsp;</td></tr></tbody>';
		$body .= '</table></td></tr><tr><td height="30">&nbsp;</td></tr></tbody></table>';
		$body .= '<p>&nbsp;</p><table border="0" width="571" height="83" align="center">';
		$body .= '<tbody><tr><td><p><span style="font-size: medium;">Received by:</span></p>';
		$body .= '<p>&nbsp;</p></td><td><p><span style="font-size: medium;">Approved by:</span></p><p>&nbsp;</p></td></tr>';
		$body .= '<tr><td><hr />Print Name</td><td><hr />Print Name</td></tr></tbody></table>';

		$bodies[0] = $body;
		$bodies[1] = $bodyText;

		return $bodies;
	}
}

// Execution
$formSubmission = new FormSubmission();
$formSubmission->run();
